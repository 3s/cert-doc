*******
Android
*******

Voraussetzungen
===============

Benötigt wird:
 * SSID des Netzes für schuleigene Geräte (hier ``WLAN-SG``)
 * Eindeutiger Gerätename (hier ``android-test``)
 * Zertifikatsdatei (hier ``android-test.pfx``)
 * Passwort für die Zertifikatsdatei
 * Computer und USB Kabel um das Zertifikat zu kopieren

Zertifikat übertragen
=====================

vom Computer
------------
Android Gerät per USB an den Computer anschließen.
Zertifikatsdatei auf die oberste Ebene des Internen Speicher oder der SD-Karte kopieren.

.. image:: /images/android/step00_copy.png


Geräteeinstellungen
===================

.. image:: /images/android/step01_start.png

1. Einstellungen aufrufen

.. image:: /images/android/step02_settings.png

2. Sicherheit anklicken

.. image:: /images/android/step03_security.png

3. Von SD-Karte installieren auswählen

.. image:: /images/android/step04_import.png

4. Zertifikatsdatei auswählen

.. image:: /images/android/step05_password.png

5. Passwort für die Zertifikatsdatei eingeben
6. Bestätigen

.. image:: /images/android/step06_name.png

7. Name für das Zertifikatspaket eingeben (frei wählbar)
8. WLAN als Verwendung auswählen
9. Bestätigen
10. Zurück in die Einstellungen

.. image:: /images/android/step07_wlan.png

11. WLAN Einstellungen

.. image:: /images/android/step08_ssid.png

12. SSID für Schuleigene Geräte auswählen (hier ``WLAN-SG``)

.. image:: /images/android/step09_wlan_settings.png

13. Erweiterte Optionen einblenden
14. TLS als EAP-Methode auswählen
15. Zertifikatspaket aus Schritt 7 auswählen
16. Gleiche Auswahl wie Schritt 15
17. Eindeutigen Gerätenamen eingeben
18. Bestätigen

Das Android Gerät ist nun mit dem WLAN Verbunden.
