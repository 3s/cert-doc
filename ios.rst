*********
Apple iOS
*********

Voraussetzungen
===============

Benötigt wird:
 * SSID des Netzes für schuleigene Geräte (hier ``WLAN-SG``)
 * Eindeutiger Gerätename (hier ``ios-test``)
 * Zertifikatsdatei (hier ``ios-test.pfx``)
 * Passwort für die Zertifikatsdatei
 * Computer
 * Temporäre WLAN Verbindung

Zertifikat übertragen
=====================

Am einfachsten ist die Übertragung der Zertifikate auf das Endgerät mittels HTTP.
Dafür muss das Gerät temporär mit einem WLAN Netz verbunden werden.

 * Webserver runterladen `https://github.com/m3ng9i/ran/releases <https://github.com/m3ng9i/ran/releases>`_ (``ran_windows_amd64.exe.zip``)
 * in den Ordner mit den Zertifikaten entpacken
 * ``ran_windows_amd64.exe`` ausführen

Möglicherweise erscheint noch ein Fenster der Windows Firewall, dort auf zulassen klicken.
In der zweiten Zeile der Ausgabe findet man die URL, mit der wir den Webserver vom iOS Gerät erreichen können (``http://10.0.56.7:8080``)
::

    2018-07-30 11:21:33 INFO: System: Ran is running on HTTP port 8080
    2018-07-30 11:21:33 INFO: System: Listening on http://10.0.56.7:8080
    2018-07-30 11:21:33 INFO: System: Listening on http://127.0.0.1:8080

Geräteeinstellungen
===================

.. image:: /images/ios/step01.png

1. Safari aufrufen

.. image:: /images/ios/step02.png

2. Hier brauchen wir die URL vom Webserver und den Namen des Zertifikats (``http://10.0.56.7:8080/ios-test.pfx``
3. URL öffnen

.. image:: /images/ios/step03.png

4. Installation des Profils Zulassen

.. image:: /images/ios/step04.png

5. Zertifikat installieren

.. image:: /images/ios/step05.png

6. Bestätigen

.. image:: /images/ios/step06.png

7. Nochmals bestätigen

.. image:: /images/ios/step07.png

8. Passwort für die Zertifikatsdatei eingeben
9. Weiter

.. image:: /images/ios/step08.png

10. Bestätigen

.. image:: /images/ios/step09.png

11. WLAN Einstellungen aufrufen

.. image:: /images/ios/step10.png

12. SSID für Schuleigene Geräte auswählen (hier ``WLAN-SG``)

.. image:: /images/ios/step11.png

13. Als Benutzername den Namen des Zertifikates eingeben
14. Identität anklicken

.. image:: /images/ios/step12.png

15. Zertifikat auswählen
16. Zurück

.. image:: /images/ios/step13.png

17. Authentifzifierungsmodus überprüfen
18. Verbinden

.. image:: /images/ios/step14.png

19. Zertifikat vertrauen

.. image:: /images/ios/step15.png

20. Das Gerät ist nun verbunden
